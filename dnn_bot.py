import tensorflow as tf
import blockchain_api
import numpy as np
from multilayer_dnn import multilayer_dnn
import pickle
import matplotlib.pyplot as plt
#import tor

# DNN model
train_test_ratio = 1.0
timespan = '1year'
mem_len = 40  # in days
predict_ahead = 5  # in days
output_size = 4
input_size = mem_len * output_size  # every input is predicted for the next timestep
learning_rate = 0.0001
num_step = 50000
error_threshold = 0  # 0.001
batch_size = 5
hidden_layers = [100, 100, 100, 100]
train_dir = 'dnn_bot_data_memlen%d_predictahead%d_numstep%d_batchsize%d_lr%f' % (mem_len, predict_ahead, num_step, batch_size, learning_rate)
blockchain_path = 'blockchain.pydat'

# session_save_path = 'dnn_bot_data_memlen30_predictahead5_numstep100000_batchsize3_lr0.000100-99999'
session_save_path = ''

# load data
blockchain = blockchain_api.load_blockchain_data(blockchain_path, norm_ord=np.inf, train_set_ratio=train_test_ratio, mem_len=mem_len)
if blockchain is None:
    blockchain = blockchain_api.Blockchain(mem_len, predict_ahead, train_test_ratio)
    blockchain.read_blockchain_data(timespan, 'https://blockchain.info/')  # https://blockchainbdgpzk.onion/ or https://blockchain.info/
    blockchain.save(blockchain_path)

# model tensors
x = tf.placeholder("float", shape=[None, input_size], name='input')
y_ = tf.placeholder("float", shape=[None, output_size], name='desired_output')
y = multilayer_dnn(x, input_size, hidden_layers=hidden_layers, output_layer=output_size)
global_step = tf.Variable(0, name='global_step', trainable=False)
cross_entropy = tf.nn.softmax_cross_entropy_with_logits(y, y_)
loss = tf.reduce_mean(cross_entropy, name='cross_entropy')
error = tf.reduce_mean(tf.sqrt((y - y_) * (y - y_)), name='training_error')
# training method
train_op = tf.train.AdamOptimizer(learning_rate).minimize(error, global_step=global_step)
# summary
ymean = tf.reduce_mean(y, name='prediction_mean')
y_mean = tf.reduce_mean(y_, name='desired_output_mean')
tf.scalar_summary(ymean.op.name, ymean)
tf.scalar_summary(y_mean.op.name, y_mean)
tf.scalar_summary(loss.op.name, loss)
tf.scalar_summary(error.op.name, error)

# start session
saver = tf.train.Saver()
with tf.Session() as session:
    summary_writer = tf.train.SummaryWriter(train_dir, graph_def=session.graph_def)
    summary_writer.add_graph(session.graph_def)
    summary_op = tf.merge_all_summaries()
    # train or load prev training
    if len(session_save_path) > 0:
        # restore previous training
        saver.restore(session, session_save_path)
        print 'Loaded session from %s' % session_save_path
        step = num_step
    else:
        # train
        session.run(tf.initialize_all_variables())
        for step in xrange(num_step):
            batch = blockchain.next_derived_train_batch(batch_size)
            feed_dict = {x: batch[0], y_: batch[1]}
            _, loss_val, error_val, _, _ = session.run([train_op, loss, error, ymean, y_mean], feed_dict=feed_dict)
            if step % 1000 == 0:
                print 'Step train %d: loss, error:' % step, loss_val, error_val
                summary_str = session.run(summary_op, feed_dict=feed_dict)
                summary_writer.add_summary(summary_str, step)
            if error_val < error_threshold:
                break
        session_save_path = saver.save(session, train_dir, global_step=step)
        print session_save_path

    blockchain.plot_price()
    # test dependence between price and the other inputs
    if train_test_ratio < 1.0:
        # sequential testing
        curr_price = float(blockchain.price[blockchain.train_test_sep_ind + predict_ahead + mem_len])  # train_test_sep is for derivative values, tho here it works
        test_times = []
        test_prices = []
        test_dat = blockchain.next_derived_test_batch()
        while test_dat[0] is not None:
            feed_dict = {x: test_dat[1][0], y_: test_dat[1][1]}
            _, loss_val, error_val, y_val = session.run([train_op, loss, error, y], feed_dict=feed_dict)
            curr_price += y_val[0][0] * float(blockchain.price_norm)
            test_times.append(test_dat[0])
            test_prices.append(curr_price)
            test_dat = blockchain.next_derived_test_batch()
        # plt.plot(test_times, test_prices, color='red')

    # predict the past
    predict_start_index = int(len(blockchain.time) * 0.8)
    test_dat = blockchain.next_derived_batch(1, None, None, predict_start_index)
    time = blockchain.get_time_at(predict_start_index)
    print test_dat
    price = blockchain.get_vals_at_time(time)[2]

    future_past_prices = []
    dtime = blockchain.time[-1] - blockchain.time[-2]
    inp_d = np.reshape(test_dat[0][0], [mem_len, output_size])
    end_time = blockchain.time[-1]
    cum_test_error = 0
    cum_test_error_counter = 0
    while time < end_time:
        dprice = session.run([y], feed_dict={x: np.reshape(np.concatenate(inp_d), [1, mem_len*output_size])})
        time += dtime
        price += dprice[0][0][0]
        future_past_prices.append((time, price * blockchain.price_norm))
        inp_d = np.concatenate((inp_d[1:], dprice[0]))
        cum_test_error += np.abs(price * blockchain.price_norm - blockchain.get_vals_at_time(time))
        cum_test_error_counter += 1
    plt.plot(*zip(*future_past_prices), color='pink')
    cum_test_error /= cum_test_error_counter
    print cum_test_error

    # predict the future
    predict_forward = 100  # days
    dtime = blockchain.time[-1] - blockchain.time[-2]
    time = blockchain.time[-1] + predict_ahead * dtime
    price = blockchain.price_normalized[-1]
    _, inp_d, _ = blockchain.latest_derived_values(mem_len)
    future_prices = []
    for p in range(predict_forward):
        dprice = session.run([y], feed_dict={x: np.reshape(np.concatenate(inp_d), [1, mem_len*output_size])})
        time += dtime
        price += dprice[0][0][0]
        future_prices.append((time, price * blockchain.price_norm))
        inp_d = np.concatenate((inp_d[1:], dprice[0]))
    plt.plot(*zip(*future_prices), color='orange')
    # save future prices
    with open(train_dir + '/future_prices_' + session_save_path[-5:] + '.pydat', "wb") as f:
        pickle.dump(future_prices, f)

    summary_writer.close()

plt.legend(['real price', 'past prediction', 'future prediction'])
plt.show()




