import json
import urllib2
import numpy as np
from numpy.linalg import norm
import matplotlib.pyplot as plt
import pickle
import os


def load_blockchain_data(bc_path, mem_len=None, predict_ahead=None, norm_ord=None, train_set_ratio=None):
    if os.path.isfile(bc_path):
        with open(bc_path, "rb") as f:
            blockchain = pickle.load(f)
            # set variables that are not None
            if mem_len is not None:
                blockchain.mem_len = mem_len
                blockchain.mem_len = mem_len
                blockchain.test_data_ind = mem_len - 1
            if predict_ahead is not None:
                blockchain.predict_ahead = predict_ahead
            if train_set_ratio is not None:
                blockchain.train_set_ratio = train_set_ratio
            # make processing of which parameters are changed
            if norm_ord is not None or predict_ahead is not None:
                blockchain.extract_data(norm_ord)
            if train_set_ratio is not None or norm_ord is not None or predict_ahead is not None:
                blockchain.gen_sep_train_test()
            return blockchain
    return None


class Blockchain:

    def __init__(self, mem_len=20, predict_ahead=1, train_set_ratio=0.8):
        # parameters
        self.mem_len = mem_len  # in days, >= 1, if 1 it only considers 1 day before
        self.train_set_ratio = train_set_ratio
        self.test_data_ind = mem_len - 1  # index of the test data that have not been tested (of inp_d_test and d_out_test)
        self.train_test_sep_ind = 0
        self.predict_ahead = predict_ahead  # number of days to predict in advance
        # basic data got from the api
        self.tradevol_pairs = None
        self.txvol_pairs = None
        self.price_pairs = None
        self.ntx_pairs = None
        # extracted/processed data
        self.time = None
        self.price = None
        self.tradevol = None
        self.txvol = None
        self.ntx = None
        self.price_norm = 0  # so it can be used later to scale price back
        self.price_normalized = None
        self.tradevol_normalized = None
        self.txvol_normalized = None
        self.ntx_normalized = None
        self.inputs = None
        self.desired_outputs = None
        self.inputs_d = None  # derivative inputs
        self.desired_outputs_d = None
        self.inp_d_train = None
        self.d_out_train = None
        self.inp_d_test = None
        self.d_out_test = None

    def read_blockchain_data(self, timespan='1year', base_url='http://blockchain.info/', norm_ord=None):
        """
        Collects relevant data from blockchain.info.http://blockchain.info/
        :param timespan: timespan of data we interested in (string) - 30days|60days|180days|1year|2year|all
        :return: tuple of B market price, B trade volume, B transaction volume, number of transaction (excluding popular addresses)
        """
        r = urllib2.urlopen(base_url + "charts/trade-volume?timespan=%s&format=json" % timespan)
        tradevol_string = r.read()
        r.close()

        r = urllib2.urlopen(base_url + "charts/estimated-transaction-volume-usd?timespan=%s&format=json" % timespan)
        txvol_string = r.read()
        r.close

        r = urllib2.urlopen(base_url + "charts/market-price?timespan=%s&format=json" % timespan)
        price_string = r.read()
        r.close

        r = urllib2.urlopen(base_url + "charts/n-transactions-excluding-popular?timespan=%s&format=json" % timespan)
        ntx_string = r.read()
        r.close

        # extract json data
        tradevol_dict = json.loads(tradevol_string)
        txvol_dict = json.loads(txvol_string)
        price_dict = json.loads(price_string)
        ntx_dict = json.loads(ntx_string)

        self.tradevol_pairs = tradevol_dict["values"]
        self.txvol_pairs = txvol_dict["values"]
        self.price_pairs = price_dict["values"]
        self.ntx_pairs = ntx_dict["values"]

        self.extract_data(norm_ord)
        self.gen_sep_train_test()
    
    def extract_data(self, norm_ord=None):
        # make numpy arrays out of the data
        self.time = np.array([t['x'] for t in self.price_pairs])
        self.tradevol = np.array([t['y'] for t in self.tradevol_pairs]).astype('float32')
        self.txvol = np.array([t['y'] for t in self.txvol_pairs]).astype('float32')
        self.price = np.array([t['y'] for t in self.price_pairs]).astype('float32')
        self.ntx = np.array([t['y'] for t in self.ntx_pairs]).astype('float32')
        # normalized versions
        self.price_norm = norm(self.price, ord=norm_ord)
        self.price_normalized = self.price / self.price_norm
        self.tradevol_normalized = self.tradevol / norm(self.tradevol, ord=norm_ord)
        self.txvol_normalized = self.txvol / norm(self.txvol, ord=norm_ord)
        self.ntx_normalized = self.ntx / norm(self.ntx, ord=norm_ord)
        # possible inputs, outputs for dnn
        self.inputs = zip(self.price_normalized[:-self.predict_ahead], self.tradevol_normalized[:-self.predict_ahead],
                          self.txvol_normalized[:-self.predict_ahead], self.ntx_normalized[:-self.predict_ahead])
        self.inputs = np.array([np.asarray(t) for t in self.inputs])
        self.desired_outputs = zip(self.price_normalized[self.predict_ahead:], self.tradevol_normalized[self.predict_ahead:],
                                   self.txvol_normalized[self.predict_ahead:], self.ntx_normalized[self.predict_ahead:])
        self.desired_outputs = np.array([np.asarray(t) for t in self.desired_outputs])
        # derivative values
        self.inputs_d = []
        self.desired_outputs_d = []
        for i in range(len(self.inputs) - 1):
            self.inputs_d.append(self.inputs[i+1] - self.inputs[i])
            self.desired_outputs_d.append(self.desired_outputs[i+1] - self.desired_outputs[i])
        self.inputs_d = np.array(self.inputs_d)
        self.desired_outputs_d = np.array(self.desired_outputs_d)

    def get_vals_at_time(self, at):
        index = np.sum(self.time < at)
        if index >= len(self.time):
            print "Warning: get_vals_at_time over index!"
            index = len(self.time) - 1
        return self.time[index], self.price[index], self.price_normalized[index], self.tradevol[index], self.txvol[index], self.ntx[index]

    def gen_sep_train_test(self):
        self.train_test_sep_ind = int(self.train_set_ratio * len(self.inputs_d))
        self.inp_d_train = self.inputs_d[:self.train_test_sep_ind]
        self.d_out_train = self.desired_outputs_d[:self.train_test_sep_ind]
        self.inp_d_test = self.inputs_d[self.train_test_sep_ind:]
        self.d_out_test = self.desired_outputs_d[self.train_test_sep_ind:]

    def save(self, to_path):
        with open(to_path, "wb") as f:
            pickle.dump(self, f)

    def next_derived_batch(self, batch_size, from_in=None, from_des_out=None, at=None):
        if from_in is None:
            from_in = self.inputs_d  # whole dataset
        if from_des_out is None:
            from_des_out = self.desired_outputs_d  # whole dataset

        if at is None:  # random
            ks = np.random.randint(self.mem_len - 1, len(from_in), size=batch_size)
            x = [np.concatenate(from_in[k-self.mem_len+1:k+1]) for k in ks]  # +1 because desired_outputs are already aligned with inputs and :k+1 is exclusive
            y = [from_des_out[k] for k in ks]
        else:
            k = at
            x = [np.concatenate(from_in[k-self.mem_len+1:k+1])]
            y = [from_des_out[k]]
        return x, y

    def next_derived_train_batch(self, batch_size):
        return self.next_derived_batch(batch_size, self.inp_d_train, self.d_out_train)

    def next_derived_test_batch(self):
        if self.test_data_ind < len(self.inp_d_test):
            res = self.next_derived_batch(1, self.inp_d_test, self.d_out_test, at=self.test_data_ind)
            self.test_data_ind += 1
            return self.get_time_at(self.test_data_ind + self.predict_ahead, True), res
        else:
            self.test_data_ind = self.mem_len - 1
            return None, None, None  # out of test cases

    def latest_values(self, history_len=1):
        return self.time[-history_len:], self.inputs[-history_len:], self.desired_outputs[-history_len:]

    def latest_derived_values(self, history_len=1):
        return self.time[-history_len:], self.inputs_d[-history_len:], self.desired_outputs_d[-history_len:]

    def get_time_at(self, at, test_time=False):
        if test_time:
            at += self.train_test_sep_ind
        if at < len(self.time):
            return self.time[at]
        return None

    def plot_price(self):
        plt.figure()
        plt.title('Bitcoin price')
        plt.xlabel('timetamp')
        plt.ylabel('price (USD)')
        plt.plot(self.time, self.price)

