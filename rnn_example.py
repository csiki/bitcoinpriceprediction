import tensorflow as tf
from tensorflow.models.rnn import rnn
from tensorflow.models.rnn.rnn_cell import BasicLSTMCell, LSTMCell
import numpy as np

if __name__ == '__main__':
    np.random.seed(1)
    size = 100
    batch_size= 100
    n_steps = 45
    seq_width = 50

    initializer = tf.random_uniform_initializer(-1, 1)

    # sequence we will provide at runtime
    seq_input = tf.placeholder(tf.float32, [n_steps, batch_size, seq_width])
    # what timestep we want to stop at
    early_stop = tf.placeholder(tf.int32)

    # inputs for rnn needs to be a list, each item being a timestep.
    # we need to split our input into each timestep, and reshape it because split keeps dims by default
    inputs = [tf.reshape(i, (batch_size, seq_width)) for i in tf.split(0, n_steps, seq_input)]

    cell = LSTMCell(size, seq_width, initializer=initializer)
    initial_state = cell.zero_state(batch_size, tf.float32)
    # set up lstm
    outputs, states = rnn.rnn(cell, inputs, initial_state=initial_state, sequence_length=early_stop)

    iop = tf.initialize_all_variables()
    session = tf.Session()
    session.run(iop)

    # early_stop can be varied, but seq_input needs to match the shape that was defined earlier
    feed = {early_stop: 100, seq_input: np.random.rand(n_steps, batch_size, seq_width).astype('float32')}

    # run once
    # output is a list, each item being a single timestep. Items at t>early_stop are all 0s
    outs = session.run(outputs, feed_dict=feed)
    #print type(outs)
    #print outs