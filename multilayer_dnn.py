import math
import tensorflow as tf


def multilayer_dnn(input, input_size, hidden_layers=[], output_layer=1):
    in_size = input_size
    for i, out_size in enumerate(hidden_layers):
        with tf.name_scope('hidden%d' % i) as scope:
            weights = tf.Variable(tf.truncated_normal([in_size, out_size], stddev=1.0 / math.sqrt(float(in_size))), name='weights')
            biases = tf.Variable(tf.zeros([out_size]), name='biases')
            input = tf.nn.relu(tf.matmul(input, weights) + biases, 'relu')  # overwrite input for next iteration
        in_size = out_size

    weights = tf.Variable(tf.truncated_normal([in_size, output_layer], stddev=1.0 / math.sqrt(float(in_size))), name='weights_out')
    biases = tf.Variable(tf.zeros([output_layer]), name='biases_out')
    return tf.matmul(input, weights) + biases
